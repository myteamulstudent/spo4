﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laboratornaya_4
{
    class Program
    {
        static void Main(string[] args)
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            if (args.Length == 0)
            {
                DirectoryInfo info = new DirectoryInfo(currentDirectory);
                var files = info.GetFiles();
                var folders = info.GetDirectories();

                var names = new List<FileSystemInfo>(files.ToList());
                names.AddRange(folders.ToList());
                foreach (FileSystemInfo file in names)
                {
                    FileAttributes attributes = file.Attributes;
                    if ((attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
                        Console.WriteLine(file.Name);
                }
            }
            else if (args[0] == "-a")
            {
                var filePaths = Directory.GetFiles(currentDirectory);

                var folderPaths = Directory.GetDirectories(currentDirectory);
                var files = filePaths.Select(p => Path.GetFileName(p));
                var folders = folderPaths.Select(p => Path.GetFileName(p));

                var names = new List<String>(files.ToList());
                names.AddRange(folders.ToList());
                foreach (var file in names)
                {
                    Console.WriteLine(file);
                }
            }
            else if (args[0] == "-l")
            {
                DirectoryInfo info = new DirectoryInfo(currentDirectory);
                var files = info.GetFiles();
                var folders = info.GetDirectories();

                var names = new List<FileSystemInfo>(files.ToList());
                names.AddRange(folders.ToList());

                names = names.Where(p => (p.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden).ToList();

                if (args.Length > 1 && args[1] == "-h")
                {
                    foreach (var file in names)
                    {
                        var length = file is FileInfo ? (file as FileInfo).Length : -1;
                        Console.WriteLine(string.Format("{0}, Size {1} KB, Created {2}, Last write {3} ", file.Name, length / 1024, file.CreationTime, file.LastWriteTime));
                    }
                }
                else
                {
                    foreach (var file in names)
                    {
                        var length = file is FileInfo ? (file as FileInfo).Length : 0;
                        Console.WriteLine(string.Format("{0}, Size {1}, Created {2}, Last write {3} ", file.Name, length, file.CreationTime, file.LastWriteTime));
                    }
                }
            }
            else if (args[0] == "-C")
            {
                DirectoryInfo info = new DirectoryInfo(currentDirectory);
                var files = info.GetFiles();
                var folders = info.GetDirectories();
                var nameLength = 15;

                var allFiles = new List<FileSystemInfo>(files.ToList());
                allFiles.AddRange(folders.ToList());

                int countElementsInLine = -1;
                foreach (FileSystemInfo file in allFiles)
                {
                    countElementsInLine++;
                    if (countElementsInLine > 3)
                    {
                        countElementsInLine = 0;
                        Console.Write(Environment.NewLine);
                    }

                    string name = string.Empty;
                    if (file.Name.Length > nameLength)
                    {
                        name = file.Name.Substring(0, 12) + "...";
                    }
                    else
                    {
                        int difference = nameLength - file.Name.Length;

                        StringBuilder sb = new StringBuilder(file.Name);
                        for (int i = 0; i < difference; i++)
                        {
                            sb.Append(" ");
                        }
                        name = sb.ToString();
                    }
                    Console.Write(name + "   ");
                }
            }
        }
    }
}
